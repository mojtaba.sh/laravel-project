<?php

$permissions = [
    'edit-user' => [
        'title' => 'edit-user',
        'fa_title' => 'ویرایش کاربران'
    ],
    'delete-user' => [
        'title' => 'delete-user',
        'fa_title' => 'حذف کاربر'
    ],
    'add-user' => [
        'title' => 'add-user',
        'fa_title' => 'افزودن کاربر'
    ],
    'view-users' => [
        'title' => 'view-users',
        'fa_title' => 'مشاهده ی کاربران'
    ],

    'delete-role' => [
        'title' => 'delete-role',
        'fa_title' => 'حذف نقش'
    ],
    'add-role' => [
        'title' => 'add-role',
        'fa_title' => 'افزودن نقش'
    ],
    'edit-role' => [
        'title' => 'edit-role',
        'fa_title' => 'ویرایش نقش ها'
    ],
    'view-roles' => [
        'title' => 'view-roles',
        'fa_title' => 'مشاهده نقش ها'
    ],
    'change-role' => [
        'title' => 'change-role',
        'fa_title' => 'تغییر نقش ها'
    ],
];

return [
    'users' => [
        'admin' => [
            'data' => [
                'email' => 'admin@test.com',
                'name' => 'admin',
            ],
            'access' => [
                'roles' => [
                    [
                        'title' => 'Administrator',
                        'fa_title' => 'مدیر ارشد',
                    ]
                ],
                'permissions' => $permissions
            ]
        ],
        'writer' => [
            'data' => [
                'email' => 'writer@test.com',
                'name' => 'writer',
            ],
            'access' => [
                'roles' => [
                    [
                        'title' => 'Writer',
                        'fa_title' => 'نویسنده',
                    ]
                ],
                'permissions' => [
                    $permissions['add-user'],
                    $permissions['view-users'],
                ]
            ]
        ],
        'support' => [
            'data' => [
                'email' => 'support@test.com',
                'name' => 'support',
            ],
            'access' => [
                'roles' => [
                    [
                        'title' => 'Support',
                        'fa_title' => 'پشتیبان',
                    ]
                ],
                'permissions' => [
                    $permissions['view-users'],
                ]
            ]
        ]
    ],

    'permissions' => $permissions,
];
