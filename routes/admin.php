<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\RoleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web,verified,is_admin" middleware group and "admin." name.
| Now create something great!
|
*/

Route::get('dashboard', [DashboardController::class,'index'])->name('dashboard');

Route::resource('users', UserController::class);

Route::post('add-roles/{role}',[RoleController::class,'addRolesToAdmins']);
Route::resource('roles', RoleController::class);

Route::resource('profile', ProfileController::class)->parameters([
    'profile' => 'user'
])->only(['show','edit','update'])->middleware('me');
