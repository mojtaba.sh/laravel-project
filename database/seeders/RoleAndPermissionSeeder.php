<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $config = config('permissions.users');

        foreach($config as $item)
        {
            $user = User::factory(array_merge($item['data'],['level' => 'admin','email_verified_at' => now()]))->create();

            $roles = [];
            foreach($item['access']['roles'] as $role)
            {
                $roles[] = Role::firstOrCreate($role)->id;
            }

            foreach($item['access']['permissions'] as $permission)
            {
                $permission = Permission::firstOrCreate($permission);
                $permission->roles()->syncWithoutDetaching($roles);
            }

            $user->roles()->syncWithoutDetaching($roles);
        }
    }
}
