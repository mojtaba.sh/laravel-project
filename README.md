## Installation

Clone the repository

    git clone https://gitlab.com/mojtaba.kh.sh/laravel-project.git

Switch to the repo folder

    cd laravel-project
  

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env (Set the database connection in .env)

  
Install all the dependencies using composer

    composer install

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Fill the database with fake information

    php artisan db:seed

run

    php artisan storage:link

Start the local development server

    php artisan serve

You can now access the server at **http://127.0.0.1:8000** and You can login with **email:admin@test.com** and **password:password** as administrator
