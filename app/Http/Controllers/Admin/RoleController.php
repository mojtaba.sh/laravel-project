<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny',Role::class);

        $roles = Role::with(['users','permissions'])->filter(['search' => $request->search])->latest()->paginate(12)->withQueryString();
        $admins = User::where('level','admin')->get();
        return view('admin.roles.index',compact('roles','admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create',Role::class);

        $permissions = Permission::all();
        return view('admin.roles.create',compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create',Role::class);

        $request->validate([
            'title' => 'required|string|max:255|unique:roles,title',
            'fa_title' => 'required|string|max:255',
            //'permissions' => 'required|array|min:1',
        ]);

        $role = Role::create([
            'fa_title' => $request->input('fa_title'),
            'title' => $request->input('title'),
        ]);

        $role->permissions()->attach($request->input('permissions'));

        return back()->with('success', 'نقش با موفقیت ایجاد شد.');;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $this->authorize('update',Role::class);

        $permissions = Permission::all();
        return view('admin.roles.edit',compact('role','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $this->authorize('update',Role::class);

        $request->validate([
            'title' => 'required|string|max:255',
            'fa_title' => 'required|string|max:255',
            //'permissions' => 'required|array|min:1',
        ]);

        $role->update([
            'fa_title' => $request->input('fa_title'),
            'title' => $request->input('title'),
        ]);

        $role->permissions()->sync($request->input('permissions'));

        return back()->with('success', 'نقش با موفقیت ویرایش شد.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $this->authorize('delete',Role::class);

        $role->permissions()->detach();
        $role->delete();

        return back();
    }

    public function addRolesToAdmins(Request $request,Role $role)
    {
        $this->authorize('change',Role::class);

        $role->users()->sync($request->input('admins'));

        return back()->with('success', ' با موفقیت انجام شد.');
    }
}
