<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rules;

class ProfileController extends Controller
{
    public function show(User $user)
    {
        return view('admin.profile.show',compact('user'));
    }

    public function edit(User $user)
    {
        return view('admin.profile.edit',compact('user'));
    }

    public function update(Request $request,User $user)
    {
        $request->validate([
            "email" => "required|string|email|max:255",
            "name" => "required|string|max:255",
            "photo" => "nullable|image",
            'password' => ['nullable', Rules\Password::defaults()],
        ]);

        $photo = $request->file('photo');

        if($photo)
        {
            if($user->photo) Storage::disk('public')->delete($user->photo);

            $user->photo = $photo->store('users/profile','public');
            $user->save();
        }

        if($request->input('password'))
        {
            $user->password = Hash::make($request->password);
            $user->save();
        }


        $user->update([
            "email"     => $request->input('email'),
            "name"      => $request->input('name'),
        ]);


        return back()->with('success', 'پروفایل با موفقیت ویرایش شد.');
    }
}
