<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rules;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny',User::class);

        $users = User::filter(['search' => $request->search])->latest()->paginate(12)->withQueryString();
        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create',User::class);

        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create',User::class);

        $request->validate([
            "email" => "required|string|email|max:255|unique:users",
            "name" => "required|string|max:255",
            "password" => ['required', 'confirmed', Rules\Password::defaults()],
            "level" => "required|in:user,admin",
            "photo" => "nullable|image"
        ]);

        $photo = $request->file('photo');
        $photo = $photo ? $photo->store('users/profile','public') : null;

        User::create([
            "email"     => $request->input('email'),
            "name"      => $request->input('name'),
            "password"  => Hash::make($request->password),
            "level"     => $request->input('level'),
            "photo"     => $photo,
        ]);

        return back()->with('success', 'کاربر با موفقیت ایجاد شد.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('update',User::class);

        return view('admin.users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->authorize('update',User::class);

        $request->validate([
            "email" => "required|string|email|max:255",
            "name" => "required|string|max:255",
            "level" => "required|in:user,admin",
            "photo" => "nullable|image"
        ]);

        $photo = $request->file('photo');

        if($photo)
        {
            if($user->photo) Storage::disk('public')->delete($user->photo);

            $user->photo = $photo->store('users/profile','public');
            $user->save();
        }


        $user->update([
            "email"     => $request->input('email'),
            "name"      => $request->input('name'),
            "level"     => $request->input('level'),
        ]);


        return back()->with('success', 'کاربر با موفقیت ویرایش شد.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('delete',User::class);

        if($user->photo) Storage::disk('public')->delete($user->photo);
        $user->delete();

        return back();
    }
}
