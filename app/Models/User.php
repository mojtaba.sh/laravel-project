<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'photo',
        'level',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'level',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeFilter($query,Array $request)
    {
        $request = collect($request);

        if($request->get('search'))
            $query->where('name','like','%'.$request->get('search').'%')->orWhere('email','like','%'.$request->get('search').'%');

        return $query;
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    public function hasRoles(array $roles)
    {
        foreach($roles as $role)
        {
            if($this->roles->contains('title',$role['title']))
            {
                return true;
            }
        }

        return false;
    }

    public function hasPermission(String $title)
    {
        $permission = Permission::where('title',$title)->first();

        if(!$permission) return null;

        $roles = $permission->roles;

        if(!$roles) return false;

        return $this->hasRoles($roles->toArray());
    }
}
