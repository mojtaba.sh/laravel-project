<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    use HasFactory;

    protected $fillable = [
        'title' , 'fa_title'
    ];

    public function users() : BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function permissions() : BelongsToMany
    {
        return $this->belongsToMany(Permission::class);
    }

    public function scopeFilter($query,Array $request)
    {
        $request = collect($request);

        if($request->get('search'))
            $query->where('title','like','%'.$request->get('search').'%')->orWhere('fa_title','like','%'.$request->get('search').'%');

        return $query;
    }

}
