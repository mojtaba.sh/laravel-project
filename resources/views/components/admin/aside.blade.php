<div class="sidebar" data-color="orange" data-image="../assets/img/sidebar-5.jpg">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="{{ route('admin.dashboard') }}" class="simple-text logo-mini">

            </a>
            <a href="{{ route('admin.dashboard') }}" class="simple-text logo-normal">
                {{ config('app.name') }}
            </a>
        </div>
        <div class="user">
            <div class="photo">
                <img src="{{ auth()->user()->photo ? asset('storage/'.auth()->user()->photo) : asset('profile.png') }}" />
            </div>
            <div class="info ">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                    <span>{{ auth()->user()->name }}
                        <b class="caret"></b>
                    </span>
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li>
                            <a class="profile-dropdown" href="{{ route('admin.profile.show',auth()->user()->id) }}">
                                <span class="sidebar-mini"></span>
                                <span class="sidebar-normal">پروفایل من</span>
                            </a>
                        </li>
                        <li>
                            <a class="profile-dropdown" href="{{ route('admin.profile.edit',auth()->user()->id) }}">
                                <span class="sidebar-mini"></span>
                                <span class="sidebar-normal">ویرایش پروفایل</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">
            <li class="nav-item {{ request()->routeIs('admin.dashboard') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('admin.dashboard') }}">
                    <i class="nc-icon nc-chart-pie-35"></i>
                    <p>داشبورد</p>
                </a>
            </li>

            @if (auth()->user()->can('viewAny', \App\Models\Role::class) || auth()->user()->can('viewAny', \App\Models\User::class))
            <li class="nav-item {{ request()->routeIs('admin.users.*') ? 'active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#componentsExamples">
                    <i class="nc-icon nc-circle-09"></i>
                    <p>
                        مدیریت کاربران
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse " id="componentsExamples">
                    <ul class="nav">
                        @can(['viewAny'],\App\Models\User::class)
                        <li class="nav-item {{ request()->routeIs('admin.users.index') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('admin.users.index') }}">
                                <span class="sidebar-mini"></span>
                                <span class="sidebar-normal">لیست کاربران</span>
                            </a>
                        </li>
                        @endcan
                        @can('viewAny',\App\Models\Role::class)
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ route('admin.roles.index') }}">
                                <span class="sidebar-mini"></span>
                                <span class="sidebar-normal">لیست نقش ها</span>
                            </a>
                        </li>
                        @endcan
                    </ul>
                </div>
            </li>
            @endif
        </ul>
    </div>
</div>
