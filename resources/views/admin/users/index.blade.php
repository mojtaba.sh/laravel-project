<x-admin.layouts>
    <div class="row">
        <div class="col-md-6">
            <h4 class="card-title">لیست کاربران</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card table-with-switches">
                <div class="card-header ">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            @can('create',App\Models\User::class)
                            <a class="btn btn-info" href="{{ route('admin.users.create') }}">افزودن کاربر جدید</a>
                            @endcan
                        </div>
                        <div>
                            <form action="{{ route('admin.users.index') }}" method="get" class="position-relative">
                                <input type="name" value="{{ request('search') }}" name="search" class="form-control form-control-sm" placeholder="جستجو...">
                                @if (request('search'))
                                <a href="{{ route('admin.users.index') }}" style="position: absolute;top: 8px;left: 10px;">x</a>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-body table-full-width">
                    @if ($users->isEmpty())
                        <div class="col">
                            <div class="alert alert-info" role="alert">
                                موردی برای نمایش وجود ندارد.
                            </div>
                        </div>
                    @else
                    <table class="table table-striped">

                        <thead>
                            <tr>
                                <th>نام</th>
                                <th>ایمیل</th>
                                <th class="text-center">سطح</th>
                                @canany(['delete','update'],App\Models\User::class)
                                <th class="text-center"></th>
                                @endcanany
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <td class="d-flex justify-content-start align-items-center align-middle">
                                    <div><img height="32" width="32" class="rounded-circle" src="{{ $user->photo ? asset('storage/' .$user->photo) : asset('profile.png') }}" alt=""></div>
                                    <div class="mx-2">
                                        <a href="{{ route('admin.users.show',$user) }}">
                                            {{ $user->name }}
                                        </a>
                                    </div>
                                </td>
                                <td>{{ $user->email }}</td>
                                <td class="text-center">
                                    @if ($user->level === 'user')
                                        <span class="badge p-2 badge-info">عادی</span>
                                    @else
                                        <span class="badge p-2 badge-success">ادمین</span>
                                    @endif
                                </td>
                                <td class="text-right">
                                    <form action="{{route('admin.users.destroy',$user->id)}}" method="post">
                                        @csrf
                                        @method('delete')
                                        @can('update',App\Models\User::class)
                                        <a href="{{ route('admin.users.edit',$user->id) }}" class="btn btn-primary btn-sm">ویرایش<i class="icon-pencil"></i></a>
                                        @endcan
                                        @can('delete',App\Models\User::class)
                                        <button class="btn btn-danger btn-sm" id="delete-item-{{ $user->id }}" ><i class="icon-trash-o "></i>حذف</button>
                                        @push('scripts')
                                            <script>
                                                $("#delete-item-{{ $user->id }}").on('click', function (event) {
                                                    event.preventDefault();
                                                    var self = $(this);
                                                    swal({
                                                        title: "مطمئنی؟",
                                                        text: "",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonClass: "btn btn-info btn-fill",
                                                        confirmButtonText: "آره",
                                                        cancelButtonText: "انصراف",
                                                        cancelButtonClass: "btn btn-danger btn-fill",
                                                        closeOnConfirm: false,
                                                    }, function() {
                                                        self.parents('form:first').submit();
                                                        swal("موفق!", "با موفقیت حذف شد.", "success");
                                                    });
                                                });
                                            </script>
                                        @endpush
                                        @endcan
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="d-flex justify-content-center align-items-center">
                        {{ $users->links() }}
                    </div>
                    @endif
                </div>
            </div>
        </div>

    </div>
</x-admin.layouts>
