<x-admin.layouts>

    <div class="row">
        <div class="col-md-12">
            <div class="card stacked-form">
                <div class="card-header d-flex justify-content-start align-items-center">
                    <img src="{{ $user->photo ? asset('storage/' . $user->photo) : asset('profile.png') }}"  height="64" width="64" class="rounded-circle mx-2" alt="">
                    <h4 class="card-title">ویرایش کاربر {{ $user->name }}</h4>
                </div>
                <div class="card-body ">
                    <form method="POST" action="{{ route('admin.users.update',$user) }}" enctype="multipart/form-data">
                        @csrf
                        @method('patch')
                        <div class="form-row">
                            <div class="form-group col-md-6 @error('name') has-error @enderror">
                                <label>نام</label>
                                <input type="text" name="name" class="form-control" value="{{ $user->name }}">
                                @error('name')
                                    <label class="error" for="name">{{ $message }}</label>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 @error('email') has-error @enderror">
                                <label>ایمیل</label>
                                <input type="email" name="email" class="form-control" value="{{ $user->email }}">
                                @error('email')
                                    <label class="error" for="name">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>سطح</label>
                                <select class="custom-select" name="level">
                                    <option value="user" {{ $user->level === 'user' ? 'selected' : '' }}>عادی</option>
                                    <option value="admin" {{ $user->level === 'admin' ? 'selected' : '' }}>ادمین</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6 @error('photo') has-error @enderror">
                                <label>عکس پروفایل</label>
                                <input type="file" name="photo" class="form-control">
                                @error('photo')
                                    <label class="error" for="name">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-fill btn-info">ثبت</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        @push('scripts')
            @if (session()->has('success'))
            <script>
                swal("پیام موفقیت!", "{{ session('success') }}", "success")
            </script>
            @endif
        @endpush
    </div>
</x-admin.layouts>
