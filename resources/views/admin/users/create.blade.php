<x-admin.layouts>

    <div class="row">
        <div class="col-md-12">
            <div class="card stacked-form">
                <div class="card-header ">
                    <h4 class="card-title">افزودن کاربر جدید</h4>
                </div>
                <div class="card-body ">
                    <form method="POST" action="{{ route('admin.users.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6 @error('name') has-error @enderror">
                                <label>نام</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                @error('name')
                                    <label class="error" for="name">{{ $message }}</label>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 @error('email') has-error @enderror">
                                <label>ایمیل</label>
                                <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                                @error('email')
                                    <label class="error" for="name">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6 @error('password') has-error @enderror">
                                <label>رمز عبور</label>
                                <input type="password" name="password" class="form-control">
                                @error('password')
                                    <label class="error" for="name">{{ $message }}</label>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label>تکرار رمز عبور</label>
                                <input type="password" name="password_confirmation" class="form-control">

                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>سطح</label>
                                <select class="custom-select" name="level">
                                    <option value="user" selected>عادی</option>
                                    <option value="admin">ادمین</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6 @error('photo') has-error @enderror">
                                <label>عکس پروفایل</label>
                                <input type="file" name="photo" class="form-control">
                                @error('photo')
                                    <label class="error" for="name">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-fill btn-info">ثبت</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        @push('scripts')
            @if (session()->has('success'))
            <script>
                swal("پیام موفقیت!", "{{ session('success') }}", "success")
            </script>
            @endif
        @endpush

    </div>
</x-admin.layouts>
