<x-admin.layouts>

    <div class="row">
        <div class="col-lg-6">
            <div class="card stacked-form">
                <div class="card-title">
                    <div class="card-header d-flex justify-content-start align-items-center">
                        <img src="{{ $user->photo ? asset('storage/' . $user->photo) : asset('profile.png') }}"  height="64" width="64" class="rounded-circle mx-2" alt="">
                        <h4>پروفایل کاربر {{ $user->name }}</h4>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="width: 100px;">نام</td>
                                <td>{{ $user->name }}</td>
                            </tr>
                            <tr>
                                <td style="width: 100px;">ایمیل</td>
                                <td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <td style="width: 100px;">سطح</td>
                                <td>{{ $user->level === 'admin' ? 'ادمین' : 'عادی' }}</td>
                            </tr>
                            @if ($user->level === 'admin')
                            <tr>
                                <td style="width: 100px;"> نقش ها</td>
                                <td>
                                    @if ($user->roles->isEmpty())
                                        -
                                    @else
                                    @foreach ($user->roles as $role)
                                    <span class="badge m-1 p-2 badge-info ">{{ $role->fa_title }} ({{ $role->title }})</span>
                                    @endforeach
                                    @endif
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</x-admin.layouts>
