<x-admin.layouts>
    <div class="row">
        <div class="col-md-6">
            <h4 class="card-title">لیست نقش ها</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card table-with-switches">
                <div class="card-header ">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            @can('create',App\Models\Role::class)
                                <a class="btn btn-info" href="{{ route('admin.roles.create') }}">افزودن نقش جدید</a>
                            @endcan
                        </div>
                        <div>
                            <form action="{{ route('admin.roles.index') }}" method="get" class="position-relative">
                                <input type="name" value="{{ request('search') }}" name="search" class="form-control form-control-sm" placeholder="جستجو...">
                                @if (request('search'))
                                <a href="{{ route('admin.roles.index') }}" style="position: absolute;top: 8px;left: 10px;">x</a>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-body table-full-width">
                    @if ($roles->isEmpty())
                        <div class="col">
                            <div class="alert alert-info" role="alert">
                                موردی برای نمایش وجود ندارد.
                            </div>
                        </div>
                    @else
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>نقش</th>
                                    <th>دسترسی ها</th>
                                    <th>کاربران</th>
                                    @canany(['create','delete','change'],App\Models\Role::class)
                                    <th></th>
                                    @endcanany
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($roles as $role)
                                <tr>
                                    <td>{{ $role->fa_title }} ({{ $role->title }})</td>
                                    <td style="max-width: 400px;">
                                        @foreach ($role->permissions as $premission)
                                            <span class="badge p-2 badge-info mb-1">
                                                {{ $premission->fa_title }}
                                            </span>
                                        @endforeach
                                    </td>
                                    <td style="max-width: 400px;">
                                        @foreach ($role->users as $user)
                                            <a href="{{ route('admin.users.show',$user) }}" class="badge p-2 badge-secondary mb-1">
                                                {{ $user->email }}
                                            </a>
                                        @endforeach
                                    </td>
                                    <td class="text-right">
                                        <form action="{{route('admin.roles.destroy',$role->id)}}" method="post">
                                            @csrf
                                            @method('delete')
                                            @can('update',App\Models\Role::class)
                                            <a href="{{ route('admin.roles.edit',$role->id) }}" class="btn btn-primary btn-sm">ویرایش<i class="icon-pencil"></i></a>
                                            @endcan
                                            @can('change',App\Models\Role::class)
                                            <span type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#add-admin" data-role="{{ json_encode($role) }}">افزودن ادمین</span>
                                            @endcan
                                            @can('delete',App\Models\Role::class)
                                            <button class="btn btn-danger btn-sm" id="delete-item-{{ $role->id }}"><i class="icon-trash-o "></i>حذف</button>
                                            @push('scripts')
                                            <script>
                                                $("#delete-item-{{ $role->id }}").on('click', function (event) {
                                                    event.preventDefault();
                                                    var self = $(this);
                                                    swal({
                                                        title: "مطمئنی؟",
                                                        text: "",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonClass: "btn btn-info btn-fill",
                                                        confirmButtonText: "آره",
                                                        cancelButtonText: "انصراف",
                                                        cancelButtonClass: "btn btn-danger btn-fill",
                                                        closeOnConfirm: false,
                                                    }, function() {
                                                        self.parents('form:first').submit();
                                                        swal("موفق!", "با موفقیت حذف شد.", "success");
                                                    });
                                                });
                                            </script>
                                        @endpush
                                            @endcan
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="d-flex justify-content-center align-items-center">
                        {{ $roles->links() }}
                        </div>
                        <div class="modal fade" id="add-admin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="POST">
                                            @csrf
                                            @method("post")
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">ادمین ها:</label>
                                                <div class="d-flex justify-content-start align-items-center">
                                                    @foreach ($admins as $admin)
                                                    <div class="custom-control custom-checkbox m-2">
                                                        <input name="admins[]" value="{{ $admin->id }}" type="checkbox" class="custom-control-input" id="admin-{{ $admin->id }}">
                                                        <label class="custom-control-label" for="admin-{{ $admin->id }}">{{ $admin->email }} ({{ $admin->name }})</label>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>

                                            <div class="modal-footer px-0">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">انصراف</button>
                                                <button type="submit" class="btn btn-primary">ثبت</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @push('scripts')
                            <script>
                                $('#add-admin').on('show.bs.modal', function (event) {
                                    var button = $(event.relatedTarget) // Button that triggered the modal
                                    var role = button.data('role') // Extract info from data-* attributes
                                    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                                    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                                    var modal = $(this)
                                    modal.find('.modal-title').text(' افزودن نقش ' + role.title + ' به ادمین ها ');
                                    modal.find('form').attr("action",`/admin/add-roles/${role.id}`);

                                    const checkbox = modal.find("input[type=checkbox]");
                                    checkbox.map((key,item) => {item.checked = false;});

                                    let users = role.users.map(item => item.id);

                                    checkbox.map((key,item) => {

                                        if(users.includes(parseInt(item.value)))
                                        {
                                            item.checked = true;
                                        }
                                    });

                                });

                            </script>

                            @if (session()->has('success'))
                            <script>
                                swal("پیام موفقیت!", "{{ session('success') }}", "success")
                            </script>
                            @endif
                        @endpush

                    @endif

                </div>
            </div>
        </div>
    </div>
</x-admin.layouts>
