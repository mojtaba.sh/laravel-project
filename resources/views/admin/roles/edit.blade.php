<x-admin.layouts>

    <div class="row">
        <div class="col-md-12">
            <div class="card stacked-form">
                <div class="card-header ">
                    <h4 class="card-title">ویرایش نقش {{ $role->fa_title }}</h4>
                </div>
                <div class="card-body ">
                    <form method="POST" action="{{ route('admin.roles.update',$role) }}" enctype="multipart/form-data">
                        @csrf
                        @method('patch')
                        <div class="form-row">
                            <div class="form-group col-md-6 @error('fa_title') has-error @enderror">
                                <label>عنوان فارسی</label>
                                <input type="text" name="fa_title" class="form-control" value="{{ $role->fa_title }}">
                                @error('fa_title')
                                    <label class="error" for="fa_title">{{ $message }}</label>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 @error('title') has-error @enderror">
                                <label>عنوان انگلیسی</label>
                                <input type="text" name="title" class="form-control" value="{{ $role->title }}">
                                @error('title')
                                    <label class="error" for="title">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group @error('permissions') has-error @enderror">
                            <label>رمز عبور</label>

                            <div class="d-flex justify-content-start align-items-center">
                                @foreach ($permissions as $permission)
                                <div class="custom-control custom-checkbox m-2">
                                    <input name="permissions[]" value="{{ $permission->id }}" {{ $role->permissions->contains($permission) ? 'checked' : '' }} type="checkbox" class="custom-control-input" id="permission-{{ $permission->id }}">
                                    <label class="custom-control-label" for="permission-{{ $permission->id }}">{{ $permission->fa_title }}</label>
                                </div>
                                @endforeach
                            </div>

                            @error('permissions')
                                <label class="error" for="name">{{ $message }}</label>
                            @enderror
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-fill btn-info">ثبت</button>
                        </div>
                    </form>
                </div>

            </div>
            @push('scripts')
                @if (session()->has('success'))
                <script>
                    swal("پیام موفقیت!", "{{ session('success') }}", "success")
                </script>
                @endif
            @endpush
        </div>
    </div>
</x-admin.layouts>
