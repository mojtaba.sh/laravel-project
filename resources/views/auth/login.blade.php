<x-guest.layouts>
    <div class="full-page  section-image" data-color="black" data-image="../../assets/img/full-screen-image-2.jpg" ;>
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="content">
            <div class="container">
                <div class="col-md-4 col-sm-6 ml-auto mr-auto">
                    <form class="form"  method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="card card-login card-hidden">
                            <div class="card-header ">
                                <h3 class="header text-center">ورود به سایت</h3>
                            </div>
                            <div class="card-body ">
                                <div class="card-body">
                                    <div class="form-group @error('email') has-error @enderror">
                                        <label>ایمیل</label>
                                        <input type="email" name="email" id="email" placeholder="ایمیل خود را وارد کنید..." class="form-control">
                                        @error('email')
                                        <label class="error" for="name">{{ $message }}</label>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('password') has-error @enderror"">
                                        <label>رمز عبور</label>
                                        <input type="password" name="password" id="password" placeholder="رمز عبور خود را وارد کنید..." class="form-control">
                                        @error('password')
                                        <label class="error" for="name">{{ $message }}</label>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label class="form-check-label d-flex justify-content-start align-items-baseline pl-4">
                                            <input class="form-check-input" type="checkbox" name="remember">
                                            <p>
                                                مرا به خاطر بسپار
                                            </p>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-warning btn-wd">ورود</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-guest.layouts>
