<x-guest.layouts>
    <!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->

    <div class="full-page register-page section-image" data-color="orange" data-image="../../assets/img/bg5.jpg">
        <div class="content">
            <div class="container">
                <div class="card card-register card-plain text-center">
                    <div class="card-header ">
                        <div class="row  justify-content-center">
                            <div class="col-md-8">
                                <div class="header-text">
                                    <h2 class="card-title">{{ config('app.name') }}</h2>
                                    <h4 class="card-subtitle">Register for free and experience the dashboard today</h4>
                                    <hr />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-5 ml-auto">
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-circle-09"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Free Account</h4>
                                        <p>Here you can write a feature description for your dashboard, let the users know what is the value that you give them.</p>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-preferences-circle-rotate"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Awesome Performances</h4>
                                        <p>Here you can write a feature description for your dashboard, let the users know what is the value that you give them.</p>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-planet"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Global Support</h4>
                                        <p>Here you can write a feature description for your dashboard, let the users know what is the value that you give them.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mr-auto">
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="card card-plain">
                                        <div class="content">
                                            <div class="form-group">
                                                <input type="text" id="name" name="name" placeholder="نام خود را وارد کنید..." class="form-control">
                                                @error('name')
                                                <label class="error text-white" for="name">{{ $message }}</label>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input type="email" id="email" name="email" placeholder="ایمیل خود را وارد کنید..." class="form-control">
                                                @error('email')
                                                <label class="error text-white" for="email">{{ $message }}</label>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input type="password" id="password" name="password" placeholder="رمز عبور خود را وارد کنید..." class="form-control">
                                                @error('password')
                                                <label class="error text-white" for="password">{{ $message }}</label>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input type="password" name="password_confirmation" id="password_confirmation" placeholder="تکرار رمز عبور..." class="form-control">
                                            </div>
                                        </div>
                                        <div class="footer text-center">
                                            <button type="submit" class="btn btn-fill btn-neutral btn-wd">ایجاد حساب کاربری</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-guest.layouts>
