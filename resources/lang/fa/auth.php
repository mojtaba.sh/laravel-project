<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'ایمیل یا رمز عبور صحیح نمیباشد.',
    'password' => 'رمز عبور صحیح نمیباشد. ',
    'throttle' => 'تعداد غیر مجاز درخواست.لطفا بعد از :seconds مجددا تلاش کنید.',

];
